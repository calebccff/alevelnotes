---
title: "1. Practical"
date: 2019-05-10T09:51:54+01:00
type: "notes"
layout: "single"
author: "Caleb Connolly"
draft: true
description: "Module 1 Q/A"
---

### These are example specimen questions for paper 1, sources at the bottom of the page

1. 1 A student wants to carry out an experiment to determine the input power to a small electric motor without using electrical meters. The motor is used to lift light loads. The efficiency of the motor is 15%. Describe how this student candetermine the input power to the motor. Your description should include:
  * the measurements taken
  * the instruments used to take the measurements
  * how the measurements are used to determine the input power to the

> Using $\text{w.d} = mgh$ to find the work done by the motor to lift the load.
> Using $P = \frac{\text{w.d}}{t}$ to find the power given the work done and the time.
> Given $P_{out} = 0.15\times P_{in}$ for this motor
> Measure:
> * Vertical displacement of load - metre ruler or tape measure
> * Mass of load (+string) - calibrated scales
> * Time taken to lift load - stopwatch
> Record data to 2 d.p.
> Calculate the work done on the load, divide the result by the time taken to lift the load and finally divide by 0.15 to find the electrical input power.

2. A student conducts an experiment to estimate the acceleration g of free fall by dropping a heavy metal ball from a high wall into a sandpit below. Describe how this student can estimate the acceleration g of free fall. Your description should include:
  * the measurements taken
  * the instruments used to take the measurements
  * how the measurements are used to estimate g
  * an explanation of why the value for g is only an estimate

> Use a tape measure to find the height of the wall. Use a stopwatch to time the drop of the ball.
> Use $s=ut+\frac{1}{2}at^2$. Rearrange to get $g=\frac{2s}{t^2}$ this can be used with the results to find g.
> The experiment:
> Mark the height on the wall where the ball will be dropped from.
> 
