---
title: "12 Research P A G"
date: 2019-04-20T16:53:41+01:00
type: "notes"
layout: "single"
draft: false
---

# Applying Archimedes principle in the context of a weather balloon
**Or how we’re sending a balloon to the ‘edge’ of space**

## Purpose
The aim of this paper is to explore the viability of sending a small weather balloon to the stratosphere and additionally produce a model to calculate the expected maximum drift during the flight, the acceleration and velocity during flight and expected terminal velocity with and without a parachute when falling back to earth.

## Archimedes principle
Archimedes principle state that the weight of fluid displaced by a submerged object is equal to the difference between the weight of the object in a vacuum and the net weight of the object in the fluid.[\[5\]](#sources)

$$W_{fd} = W_v - W_f$$
Where:
$W_{fd}$ is the weight of the displaced fluid,
$W_v$ is the weight of the object in a vacuum and
$W_f$ is the weight of the object in the fluid (The weight - the upthrust in the fluid)

## Bouyancy force
The bouyancy force for a fluid is:
$F_B = W$ where $W$ is the weight of the fluid (in a vacuum)
The weight of the fluid can be calulcated with $W = \rho \times V \times g$ where $\rho$ is the density of the fluid $V$ is the volume of the submerged part of the object and $g$ is acceleration due to gravity ($g \approx 9.81ms^{-2}$)

### Net force
The net force on the submerged object is equal to the difference between weight of the object in vacuum and the bouyancy force. I will refer to the net force as the **upward force** from now.

## Calculating the density of the fluid
The specific focus of this paper is on balloons in air, therefore this equation is specific to air and can't be applied to other fluids (like water).[\[1\]](#sources)
$$\rho_{air} = \frac{P_{air}}{R\times T}$$
Where:
$P_{air}$ is the air pressure
$R$ is the specific gas constant for air ($287.058 JK^{-1}kg^{-1}$)
$T$ is the temperaure ($\circ K$)

## The key properties of a helium balloon.
In order to calculate the bouyancy force on a helium balloon in the atmosphere there are several key properties to account for:

### 1. The changing pressure
> As the balloon rises through the atmosphere the air pressure decreases, this means the bouyancy equation has to account for varying pressure. This can be done easily using the approximation:
> $$P = 101325(1 -[2.25577\times 10^{-5}\times h])^{5.25588}$$
> where $h$ is the altitude above sea level
> This results in the density of the air varying.
[\[2\]](#sources)
### 2. Changing temperature
> The temperature affects the density of the air and the helium. Unfortunately there is no equation to model temperature change with altitude, due to the massive changes in weather with altitude. However, the change in temperature ($\Delta T$) is most likely not more than $70\circ K$, as the temperature will not go below $-55\circ C$ or above $15\circ C$ (depending on weather).
> The theoretical maximum error in temperature is $70/273.15*100=25.6\%$, this can be reduced in several ways.

#### Methods to reduce error in temperature
> As the balloon will not go beyond the stratosphere it is much easier to estimate the temperature change - there is a linear correlation between temperature and altitude up to 10,000m [\[3\]](#sources). Followed by  ~$10,000$m of a constant temperature ($-55\degree C$) and finally a linear increase in temperature from $-50\degree C$ to  $-45\degree C$ at $30,000$m. The balloon is not designed to go above $30$km so it isn't necessary to model the temperature beyond this altitude.
> ![graph of temperature with altitude][temperature with altitude image]
> Therefore it makes sense to model the temperature as a series of equations, or more simply a single equation, a linear equation to link the points $(15, 0),\; (-55, 20,000)$ would reduce the error the the difference ebtween the temperature model and the actual temperature (A maximum $\Delta T$ of ~$10\circ K$) and thus reducing the error to $10/273.15*100=3.66\%$.
##### Final equation for temperature
$$
T = \frac{h-4285.714}{-285.714}
$$
> This equation is a rough model for the temperature and is not accurate.

### 3. Changing volume
>As a result of the change in pressure and temperature, the volume of the balloon will also change. This can be accurately modelled by the ideal gas law: [\[4\]](#sources)
> $$PV = nRT$$
> $P =$ pressure in the balloon
> $V =$ volume of the balloon
> $n =$ number of atoms in the balloon
> $R =$ specific gas constant of helium
> $T =$ temperature of the balloon (will assume perfect conductivity and use temperature of the atmosphere)
> And neatly ties the pressure, temperature and volume together, allowing a final equation to be produced.

## Conclusion
This research has allowed me to better understand the parameters that affect the flight of a weather balloon. I'm able to apply this knowledge to the real world and gather data to verify the theory.

# Equations
$$F_B=g(\frac{P}{R_{specific}\times T_1}-\frac{M_0\;T_0}{V_0T_1e^{cy}})\,(V_0(\frac{T_1}{T_0})e^{cy})$$
$$ F_B = \rho g V $$
$$ \rho = \rho_{air}-\rho_{inBalloon}$$
$$ \rho_{air} = \frac{P}{R_{specific} T} $$
$$\rho_{inBalloon}=\frac{M_0}{V} $$
$$ V = V_0{(\frac{T_1}{T_0}})e^{cy}$$
$$ V_0 = \frac{4}{3} \pi r_0^3$$

$$ M_0 = \rho_{He}V_0$$
$$ c = \frac{\rho_0g}{P_0} $$

>$R_{specific} = 287.058 JK^{-1}kg^{-1}$
$F_B =$ Force of Buoyancy
$g =$ Accelleration due to gravity, which equals 9.81$ms^{-2}$
$\rho_{air} =$ Density of air, which equals 1.225$kgm^{-3}$
$V_0 =$ The volume at the start, measured in $m^3$
$M_0 =$ Mass at the beginning in kg 
$\rho_{He} = 0.1785$
$T_0 =$ Temperature at ground level, which the standard 288.15$K$
$T_1 =$ Temperature at a given height, measured in $K$
$c = \frac{\rho_0g}{P_0}$
Where $P_0 =$ air pessure at ground, 101325$Pa$
$y =$ The altitude of the balloon.
$r_0 =$ The radius of the balloon on the ground. 


## Sources
\[1\] - https://www.grc.nasa.gov/WWW/BGH/fluden.html
Accessed 2019/04/28

\[2\] - https://www.engineeringtoolbox.com/air-altitude-pressure-d_462.html
Accessed 2019/05/12

\[3\] - https://sriutami88.wordpress.com/2012/02/28/temperature-in-the-mesosphere/
Accessed 2019/04/25

\[4\] - http://www.westfield.ma.edu/cmasi/gen_chem1/Gases/ideal%20gas%20law/pvnrt.htm
Accessed 2019/05/12

\[5\] - https://www.khanacademy.org/science/physics/fluids/buoyant-force-and-archimedes-principle/a/buoyant-force-and-archimedes-principle-article
Accessed 2019/05/14

[temperature with altitude]: https://sriutami88.wordpress.com/2012/02/28/temperature-in-the-mesosphere/

[temperature with altitude image]: https://sriutami88.files.wordpress.com/2012/02/mesosphere_temperature_graph_big.gif