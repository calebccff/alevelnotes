---
title: "3.1 Motion"
date: 2019-04-28T15:47:14+01:00
type: "notes"
layout: "single"
author: "Caleb Connolly"
draft: true
---

## Kinematics
> Understand the terms **displacement**, **instantaneous speed**, **velocity**, **average speed** and **acceleration**.
### Displacement
Displacement is the distance between the inital position of an object and the current position. It is independ of motion or the history of the object.
### Instantaneous speed
The speed of an object at a given point in time, this can only be found theoretically as their is technically not way to measure speed at a precise moment in time, it has to be over some time period.
### Velocity
The velocity of an object encodes it's speed and the direction of travel, this can be one dimensional where the negative sign represent the object travelling in the opposite direction (to that which you have taken to be positive) or in more dimensions as a vector.
### Average speed
The average speed of an object can be calculated given the distance trvelled and the time taken to travel that distance.

$$v=\frac{d}{t}$$
Where:
$v$ is the average speed
$d$ is the distance travelled
$t$ is the time taken to travel.

If velocity is constant this will give us the actual speed however if it is changing then it will give us the average speed.

### Methods to determine speed
* Stopwatch, manually controlled
* Light gates
  * Two light gates, one for start and one for stop
  * One light gate, starting when the object enters the beam and stopping when the object leaves it.
* Ticker timer - Objects pulls tape through a ticker and the distance between dots is measured.

### Notes on velocity:
* The magnitude is the *speed* of the object, direction is $\arctan(v_x, v_y)$.

## Linear motion (SUVAT)
$$v=u+at$$
$$s=\frac{1}{2}(u+v)t$$
$$s=ut+\frac{1}{2}at^2$$
$$v^2=u^2+2as$$
$v$ = final velocity
$u$ = initial velocity
$a$ = acceleration
$t$ = time
$s$ = displacement
This variables are highly dependant on context however it is generally obvious which is which in a question.

### Acceleration of free fall
$g = 9.81$
#### Experiment to calculate $g$
**Equipment**:
* Steel ball
* Scales
* Electromagentic trapdoor
* Electronic timer
**Method:**
1. Set up a circuit with the timer and electromagnet connected to the timer such that when the timer is started it will cause the ball to drop, and when the ball hits the plate at the bottom the timer will stop.
2. 